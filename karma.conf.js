require('babel-polyfill');

require('webpack');
require('path');

module.exports = (config) => {
    config.set({

        browsers: ['PhantomJS'],

        singleRun: !!process.env.CI,

        frameworks: ['mocha'],

        client: {
            captureConsole: true,
            mocha: {
                bail: true
            }
        },

        files: [
            './node_modules/phantomjs-polyfill/bind-polyfill.js',
            'tests.webpack.js'
        ],

        preprocessors: {
            'tests.webpack.js': ['webpack', 'sourcemap']
        },

        reporters: ['mocha', 'junit'],
        junitReporter: {
            outputDir: 'testreports',
            useBrowserName: true
        },

        plugins: [
            require('karma-webpack'),
            require('karma-mocha'),
            require('karma-mocha-reporter'),
            require('karma-junit-reporter'),
            require('karma-phantomjs-launcher'),
            require('karma-sourcemap-loader')
        ],

        webpack: {
            devtool: 'inline-source-map',
            module: {
                loaders: [
                    {
                        test: /\.jsx?$/,
                        loader: 'babel',
                        exclude: /node_modules/,
                        include: __dirname,
                        query: {
                            presets: ['es2015', 'stage-0', 'react'],
                            plugins: ['transform-decorators-legacy']
                        }
                    },
                    {
                        test: /\.scss$/,
                        loader: 'style!css?sourceMap!sass?sourceMap'
                    },
                    {
                        test: /.*\.(gif|png|jpe?g|svg)$/i,
                        loaders: [
                            'file?hash=sha512&digest=hex&name=[hash].[ext]',
                            'image-webpack?{progressive:true, optimizationLevel: 7,' +
                            'interlaced: false, pngquant:{quality: "65-90", speed: 4}}'
                        ]
                    }
                ]
            },
            resolve: {
                modulesDirectories: [
                    'node_modules'
                ],
                extensions: ['', '.json', '.js', '.jsx']
            }
        },

        webpackServer: {
            noInfo: true
        }

    });
};
