import React from 'react';
import { IndexRoute, Route } from 'react-router';

import {
    Main,
    About,
    Contact,
    Disclaimer,
    Home,
    NotFound,
    Work,
    Gumtree,
    Yell,
    OpenToExport,
    Rocket,
    Battleships,
} from '../pages';

export default (

    <Route path="/" component={Main}>
        <IndexRoute component={Home} />
        <Route path="about" component={About} />
        <Route path="contact" component={Contact} />
        <Route path="disclaimer" component={Disclaimer} />
        <Route path="work" component={Work} />
        <Route path="work/gumtree" component={Gumtree} />
        <Route path="work/yell" component={Yell} />
        <Route path="work/opentoexport" component={OpenToExport} />
        <Route path="work/rocket" component={Rocket} />
        <Route path="work/battleships" component={Battleships} />
        <Route path="*" component={NotFound} status={404} />
    </Route>

);
