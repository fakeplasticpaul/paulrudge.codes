const htmlString = (html, initialState, head, assets) => {
    let cssString = '';
    let jsString = '';

    Object.keys(assets.javascript).map((script) => {
        jsString += `<script src="${assets.javascript[script]}"></script>`;
        return jsString;
    }
    );

    if (jsString === '') {
        jsString = '<script src="/dist/bundle.js"></script>';
    }

    Object.keys(assets.styles).map((style) => {
        cssString += `<link rel="stylesheet"  href="${assets.styles[style]}">`;
        return cssString;
    });

    if (cssString === '') {
        cssString = '<link rel="stylesheet" href="/dist/styles.css">';
    }

    return `
    <!doctype html>
    <html lang="utf-8">
        <head>
            ${head.title.toString()}
            ${head.meta.toString()}
            ${head.link.toString()}
            ${cssString}
            <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
            <link rel="manifest" href="/manifest.json">
            <meta
                name="google-site-verification"
                content="i__6x5aMDrZ7hXgSEBpoPpm6Dh9x1VgxF_DlBmrvlRI"
            />
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0">
        </head>
    <body>
        <div class="container">${html}</div>
        <script>window.$REDUX_STATE = ${initialState}</script>
        ${jsString}
    </body>
    </html>`;
};

export default htmlString;
