import React from 'react';
import Helmet from 'react-helmet';
import { renderIntoDocument } from 'react-addons-test-utils';
import html from '../html';
import { expect } from 'chai';


describe('Html', () => {
    it('Make sure the html string is generated correctly', () => {
        Helmet.canUseDOM = false;

        renderIntoDocument(<div><Helmet
            title={"Test Title"}
        /></div>);

        const head = Helmet.rewind();

        const htmlString = html('<h1>Hello</h1>', '{1:2}', head, { javascript: [], styles: [] });
        expect(htmlString).to.have.string('<div class="container"><h1>Hello</h1></div>');
        expect(htmlString).to.have.string('<script>window.$REDUX_STATE = {1:2}</script>');
        expect(htmlString).to.have.string('<script>window.$REDUX_STATE = {1:2}</script>');
        expect(htmlString).to.have.string('<title data-react-helmet="true">Test Title</title>');
    });
});
