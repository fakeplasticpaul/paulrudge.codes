import { createStore, applyMiddleware, compose } from 'redux';
import analyticsMiddleware from '../middlewares/AnalyticsMiddleware';
import combinedReducers from '../reducers';

const enhancer = compose(
    applyMiddleware(analyticsMiddleware),
    typeof window === 'object' && typeof window.devToolsExtension !== 'undefined' ?
        window.devToolsExtension() : f => f
);

export default function configureStore(initialState = undefined) {
    const store = createStore(combinedReducers, initialState, enhancer);

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers'); // eslint-disable-line global-require
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}
