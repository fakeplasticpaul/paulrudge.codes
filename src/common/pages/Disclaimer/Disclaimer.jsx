import React, { Component } from 'react';
import Helmet from 'react-helmet';

class Disclaimer extends Component {
    render() {
        return (
            <section className="o-layout__container">
                <Helmet
                    title="PaulRudge.codes - Disclaimer"
                />
                <main role="main" className="o-layout__full">
                    <h1>
                        Disclaimer
                    </h1>
                    <p>The opinions expressed on this website, on <a
                        href="https://twitter.com/fakeplasticpaul" target="_blank"
                    > my Twitter</a> profile or any other social network are my own.
                       They do not necessarily reflect the opinions of my
                       employer, Gumtree.
                    </p>
                </main>
            </section>
        );
    }

}

export default Disclaimer;
