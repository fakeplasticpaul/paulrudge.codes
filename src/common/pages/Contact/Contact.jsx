import React, { Component } from 'react';
import Helmet from 'react-helmet';

class Contact extends Component {
    render() {
        const postboxImage = require('./postbox.png');
        return (
            <div className="o-layout__container">
                <main role="main" className="o-layout__main">
                    <Helmet
                        title="PaulRudge.codes - Contact"
                    />
                    <h1>Contact</h1>
                    <p>
                        Get in touch with me if you like what
                        I've done with my site, or if you would like more
                        information on any of the work
                        that I've been involved with.
                    </p>

                    <p>You can contact via <a
                        href="https://twitter.com/fakeplasticpaul"
                        target="_blank"
                    >
                        Twitter
                    </a> or <a href="https://uk.linkedin.com/in/paulrudge" target="_blank">
                        Linkedin
                    </a></p>

                </main>

                <aside className="o-layout__secondary">
                    <div className="u-text--center">

                        <img role="presentation" src={postboxImage} />
                    </div>
                </aside>
            </div>
        );
    }

}

export default Contact;
