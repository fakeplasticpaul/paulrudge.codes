import React from 'react';
import ReactDOM from 'react-dom';
import Page from '../Main/Main';
import { renderIntoDocument } from 'react-addons-test-utils';
import { expect } from 'chai';

describe('Home page', () => {
    const renderedPage = renderIntoDocument(<div>
        <Page store><div className="testChild">Test child</div></Page>
    </div>);
    const dom = ReactDOM.findDOMNode(renderedPage);

    it('should render correctly', () => expect(renderedPage).to.be.ok);

    it('Should render the child', () => {
        const pageTestChild = dom.getElementsByClassName('testChild');
        expect(pageTestChild.length).to.equal(1);
        expect(pageTestChild[0].innerText).to.equal('Test child');
    });

    it('Should render the header component', () => {
        const header = dom.getElementsByClassName('c-header');
        expect(header.length).to.equal(1);
    });

    it('Should render the footer component', () => {
        const footer = dom.getElementsByClassName('c-footer');
        expect(footer.length).to.equal(1);
    });
});
