import React from 'react';
import ReactDOM from 'react-dom';
import Page from '../Work/Other/Battleships';
import { renderIntoDocument } from 'react-addons-test-utils';
import { expect } from 'chai';

describe('Work Battleships page', () => {
    const renderedPage = renderIntoDocument(<div><Page /></div>);

    const dom = ReactDOM.findDOMNode(renderedPage);

    it('should render correctly', () => expect(renderedPage).to.be.ok);

    it('should render with correct amount of links', () => {
        const pageH1 = dom.getElementsByTagName('h1');
        expect(pageH1.length).to.equal(1);
        expect(pageH1[0].innerText).to.equal('Battleships');
    });
});
