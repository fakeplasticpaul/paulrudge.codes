import React, { Component } from 'react';
import TileNav from '../../components/TileNav/TileNav';
import { Link } from 'react-router';

class Home extends Component {
    render() {
        const meImage = require('./me.png');// eslint-disable-line global-require
        return (
            <section className="o-layout__container">
                <main role="main" className="o-layout__main">

                    <h1>Welcome</h1>
                    <p>
                        This is a portfolio site with information about some of my more recent work,
                        as well as my personal details.
                    </p>

                    <p>
                        If you would like to know more details about
                        anything on this site, please feel free to <Link
                            to={'/contact'}
                        >
                            contact me
                        </Link>
                    </p>


                </main>

                <div className="o-layout__secondary">
                    <div className="u-text--center">
                        <img role="presentation" src={meImage} />
                    </div>
                </div>

                <aside className="o-layout__full">
                    <h2>Selected work</h2>

                    <nav>
                        <ul className="o-layout__tiles o-bare-list">
                            <li className="o-layout__tile">
                                <TileNav
                                    to={'/work/gumtree'}
                                    title="Gumtree"
                                    name="gumtree"
                                />
                            </li>
                            <li className="o-layout__tile">
                                <TileNav
                                    to={'/work/rocket'}
                                    title="Rocket, paper, scssors"
                                    name="rock"
                                />
                            </li>
                        </ul>
                    </nav>
                </aside>

            </section>
        );
    }

}

export default Home;
