import React, { Component, PropTypes } from 'react';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Helmet from 'react-helmet';

require('../../../client/assets/scss/main.scss');

export default class Main extends Component {

    static contextTypes = {
        store: React.PropTypes.object.isRequired,
    };

    render() {
        return (
            <div className="o-layout__body">
                <Helmet
                    title="PaulRudge.codes"
                    meta={[
                    { name: 'description', content: 'paulrudge.code is my portfolio of web design' +
                     'and frontend development work that I have done' },
                    { name: 'keywords', content: 'Portfolio, Front end development, redux, react,' +
                     'js, es2015, bem, sass, oocss' }
                    ]}
                />

                <Header />
                {this.props.children}
                <Footer />
            </div>
        );
    }
}

Main.propTypes = {
    children: PropTypes.object.isRequired
};
