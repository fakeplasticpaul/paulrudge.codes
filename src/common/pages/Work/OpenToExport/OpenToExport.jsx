import React, { Component } from 'react';
import { Link } from 'react-router';
import Helmet from 'react-helmet';
import WorkImage from '../../../components/WorkImage/WorkImage';


class OpenToExport extends Component {
    render() {
        const openToExportImage1 = require('./open-to-export-large.jpg');
        const openToExportImage2 = require('./open-to-export-large2.jpg');
        const openToExportImage3 = require('./open-to-export-large3.jpg');
        return (
            <div>
                <Helmet
                    title="PaulRudge.codes - Portfolio - Open to export"
                />

                <section className="o-layout__container">

                    <main role="main" className="o-layout__main">
                        <Link to={'/work'}>Back to work</Link>

                        <h1>Open to export</h1>
                        <p>
                            Open to Export is a community driven service for small and medium
                            sized businesses, looking for help and support in exporting from the UK.
                        </p>

                        <p>
                            I worked on this project from its initial wireframing/design and
                            definition stage right through to its release to live.
                        </p>

                        <WorkImage
                            image={openToExportImage1}
                            caption="The homepage"
                        />

                        <WorkImage
                            image={openToExportImage2}
                            caption="A Topics section"
                        />

                        <WorkImage
                            image={openToExportImage3}
                            caption="Questions and answers on exporting"
                        />

                    </main>
                    <aside role="complementary" className="o-layout__secondary">
                        <h2>More Info</h2>
                        <ul>
                            <li>
                                <a target="_blank" href="/pdf/opentoexportWireframes.pdf">
                                    Wireframes
                                </a>
                            </li>
                        </ul>

                        <h2>Tech used</h2>
                        <ul>
                            <li>Freemarker</li>
                            <li>Spring MVC</li>
                            <li>HTML5</li>
                            <li>CSS3</li>
                            <li>Javascript</li>
                            <li>jQuery</li>
                        </ul>
                    </aside>
                </section>
            </div>
        );
    }

}

export default OpenToExport;
