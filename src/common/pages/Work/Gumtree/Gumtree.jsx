import React, { Component } from 'react';
import { Link } from 'react-router';
import Helmet from 'react-helmet';
import WorkImage from '../../../components/WorkImage/WorkImage';

class Gumtree extends Component {
    render() {
        const gumtreeImage1 = require('./gumtree-1.jpg');

        return (
            <div>
                <Helmet
                    title="PaulRudge.codes - Portfolio - Gumtree"
                />

                <section className="o-layout__container">

                    <main role="main" className="o-layout__main">
                        <Link to={'/work'}>Back to work</Link>

                        <h1>Gumtree</h1>
                        <p>Gumtree is where I currently work and I have worked on a lot of
                            different projects over the years I have been here. I have
                            called out some of the more major ones below.
                        </p>

                        <h2>The move to responsive</h2>

                        <WorkImage
                            image={gumtreeImage1}
                            caption="The hub page which displayed a preview of what your
                                    advert will look like"
                        />

                        <p>
                            One of the first and largest projects I worked on during
                            my time at Gumtree was the migration of all the public
                            facing sites to Responsive.
                        </p>

                        <p>
                            As the first company within the ebay classifieds group to
                            make this transition, the project involved developers
                            from five different companies. The aim of this was to spread
                            the knowledge around ready for everyone else to make the same
                            changes.
                        </p>

                        <p>
                            Being the most senior frontend developer from Gumtree on this
                            meant I led the frontend aspects of the project and was
                            responsible for driving the architectural discussions and
                            making sure everyone and everything was co-ordinated.
                        </p>

                        <p>
                            It was great to work on a project with so many talented people
                            from across the globe. It was a great learning experience for
                            everyone and a lot of fun.
                        </p>

                        <h2>Introducing the frontend build system</h2>

                        <p>
                            When I joined Gumtree all of the frontend assets were
                            being generated by a maven task. A full rebuild of the
                            entire java application was needed to make any changes,
                            generated files were being checked in to the repo, and
                            the few tests they did have were manually triggered only
                            and very much failing.
                        </p>

                        <p>This obviously was not good enough.</p>

                        <p>
                            I made a case for modernisation and got buy in from the
                            company. And so began the transition to using Grunt.
                        </p>

                        <p>
                            The new build process was integrated into the jenkins
                            pipeline. JS testing, JS linting, CSS linting and a
                            number of other code quality tools were implemented
                            and setup to generate reports of issues if anything
                            was to fail.
                            Test coverage on our js was increased to 70% and
                            monitoring tools were put in place to keep track of
                            that level.
                        </p>

                        <p>
                            The speed in which development was possible was greatly
                            increased thanks to the project and the trust in the
                            code base was greatly improved.
                        </p>

                    </main>
                    <aside role="complementary" className="o-layout__secondary">
                        <h2>More Info</h2>
                        <ul>
                            <li>
                                <a target="_blank" href="https://www.gumtree.com">
                                    Gumtree
                                </a>
                            </li>
                        </ul>
                        <h2>Tech used</h2>
                        <ul>
                            <li>Freemarker</li>
                            <li>HTML</li>
                            <li>CSS</li>
                            <li>Javascript</li>
                            <li>React</li>
                            <li>Grunt</li>
                        </ul>
                    </aside>
                </section>
            </div>
        );
    }

}

export default Gumtree;
