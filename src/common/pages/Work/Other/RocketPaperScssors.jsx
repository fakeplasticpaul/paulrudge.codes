import React, { Component } from 'react';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

class Rocket extends Component {
    render() {
        return (
            <div>
                <Helmet
                    title="PaulRudge.codes - Portfolio - Rocket Paper Scssors"
                />
                <section className="o-layout__container">

                    <main role="main" className="o-layout__full">
                        <Link to={'/work'}>Back to work</Link>

                        <h1>Rock(et), paper, (scss)ors</h1>
                        <p>
                            One evening on my train home from work I was talking with a friend
                            about a js challenge they had run, to build the game
                            <a href="https://en.wikipedia.org/wiki/Rock-paper-scissors" target="_parent">
                                "Rock, Paper, Scissors"
                            </a>. I made an off the cuff comment that I believed I could build it
                            without any js at all, using purely css and js.
                        </p>
                        <p>
                            This codepen is the successful result of that conversation.
                        </p>

                        <iframe height="565" scrolling="no" src="//codepen.io/fakeplasticpaul/embed/preview/mlwKu/?height=565&theme-id=0&default-tab=result&embed-version=2" frameBorder="no" allowTransparency="true" allowFullScreen="true" className="c-iframe--fullwidth">See the Pen <a href="http://codepen.io/fakeplasticpaul/pen/mlwKu/">Rock(et), paper, (scss)ors</a> by Paul Rudge (<a href="http://codepen.io/fakeplasticpaul">@fakeplasticpaul</a>) on <a href="http://codepen.io">CodePen</a>.</iframe>

                    </main>
                </section>
            </div>
        );
    }

}

export default Rocket;
