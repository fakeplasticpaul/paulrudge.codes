import React, { Component } from 'react';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

class Battleships extends Component {
    render() {
        return (
            <div>
                <Helmet
                    title="PaulRudge.codes - Portfolio - Battleships"
                />
                <section className="o-layout__container">

                    <main role="main" className="o-layout__full">
                        <Link to={'/work'}>Back to work</Link>

                        <h1>Battleships</h1>

                        <p>After the success I had
                            with <a href="/work/rocket">"Rock, Paper, Scissors"</a> I
                            wanted to see if I could take the concept of css only game any further.
                        </p>

                        <p>This is the result of that experiment</p>

                        <iframe className="c-iframe--fullwidth" height="565" scrolling="no" src="//codepen.io/fakeplasticpaul/embed/LpWPga/?height=565&theme-id=0&default-tab=result&embed-version=2" frameBorder="no" allowTransparency="true" allowFullscreen="true">See the Pen <a href="http://codepen.io/fakeplasticpaul/pen/LpWPga/">Battleship</a> by Paul Rudge (<a href="http://codepen.io/fakeplasticpaul">@fakeplasticpaul</a>) on <a href="http://codepen.io">CodePen</a>.</iframe>

                    </main>
                </section>
            </div>
        );
    }

}

export default Battleships;
