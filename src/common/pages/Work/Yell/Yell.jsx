import React, { Component } from 'react';
import WorkImage from '../../../components/WorkImage/WorkImage';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

class Yell extends Component {

    render() {
        const yellMapsLarge = require('./yell-maps-large.jpg');
        const yellMapsLarge2 = require('./yell-maps-large2.jpg');
        const yellMapsLarge3 = require('./yell-maps-large3.jpg');
        const yellMapsLarge4 = require('./yell-maps-large4.jpg');
        const yellMapsLarge5 = require('./yell-maps-large5.jpg');
        const yellMapsLarge6 = require('./yell-maps-large6.jpg');
        return (
            <div>
                <Helmet
                    title="PaulRudge.codes - Portfolio - Yell Maps"
                />
                <section className="o-layout__container">

                    <main role="main" className="o-layout__main">

                        <Link to={'/work'}>Back to work</Link>


                        <h1>Yell Maps</h1>
                        <p>
                            I was the lead front end developer/designer for production of the
                            Yell.com
                            maps site which was the largest javascript project ever undertaken by
                            Yell.
                        </p>

                        <WorkImage
                            image={yellMapsLarge}
                            caption="Maps with point of interest selected"
                        />

                        <p>
                            I started the project by researching all the different map supplier
                            options available
                            to us as well producing a report on all the areas of yell.com that were
                            currently
                            making use of maps so we could properly scope out all parts of the
                            project.
                        </p>

                        <WorkImage
                            image={yellMapsLarge2}
                            caption="Maps directions"
                        />

                        <p>
                            I built prototypes using MultiMap which was our current map supplier at
                            the time.
                            I then worked with the product owners and stakeholders to help define
                            all the
                            different parts of the project and what direction we should take.
                        </p>

                        <WorkImage
                            image={yellMapsLarge5}
                            caption="Maps with C3 3d maps"
                        />

                        <p>
                            To help test out the feature set and to make sure we where covering all
                            bases from
                            a user point of view I moved on to creating user flow diagrams and
                            wireframes.
                            The end result of this then helped to tweak the backlog to be ready to
                            move into development.
                        </p>

                        <WorkImage
                            image={yellMapsLarge6}
                            caption="Maps with C3 Streetview"
                        />

                        <p>
                            The company decided we would work with two third parties, Tridoo (for 2d
                            maps) and
                            C3 (for streetview and 3d maps). I worked closely with both these
                            companies when
                            implementing our solution, and helped drive new features back into there
                            API's as
                            required.
                        </p>

                        <WorkImage
                            image={yellMapsLarge3}
                            caption="Maps with business results"
                        />

                        <p>
                            The project was a massive success with us seeing a rise in maps use of
                            544% over
                            the first year. For the second time I received a gold award (Yell's
                            yearly internal
                            award system for exceptional work) for my efforts on this project.
                        </p>


                        <WorkImage
                            image={yellMapsLarge4}
                            caption="Maps showing a business information popu"
                        />

                    </main>
                    <aside
                        role="complementary"
                        className="o-layout__secondary"
                        role="complementary"
                    >
                        <h2>More Info</h2>
                        <ul>
                            <li>
                                <a target="_blank" href="/pdf/mapsDesignDocument102.pdf">
                                    Maps Wireframes
                                </a>
                            </li>
                            <li><a target="_blank" href="http://www.yell.com/maps">Yell.com maps</a>
                            </li>
                        </ul>

                        <h2>Tech used</h2>
                        <ul>
                            <li>JSP</li>
                            <li>HTML</li>
                            <li>Javascript</li>
                            <li>Wireframes</li>
                            <li>Design</li>
                        </ul>
                    </aside>
                </section>
            </div>
        );
    }

}

export default Yell;
