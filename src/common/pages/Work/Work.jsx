import React, { Component } from 'react';
import TileNav from '../../components/TileNav/TileNav';
import Helmet from 'react-helmet';

class Work extends Component {
    render() {
        return (
            <section className="o-layout__container">
                <Helmet
                    title="PaulRudge.codes - Portfolio"
                />

                <main role="main" className="o-layout__full">

                    <h1>Selected work</h1>

                    <nav role="navigation" aria-label="Selected work">
                        <ul className="o-layout__tiles o-bare-list">
                            <li className="o-layout__tile">
                                <TileNav
                                    to={'/work/gumtree'}
                                    title="Gumtree"
                                    name="gumtree"
                                />
                            </li>
                            <li className="o-layout__tile">
                                <TileNav
                                    to={'/work/rocket'}
                                    title="Rocket, paper, scssors"
                                    name="rock"
                                />
                            </li>
                            <li className="o-layout__tile">
                                <TileNav
                                    to={'/work/battleships'}
                                    title="Battleships"
                                    name="battleship"
                                />
                            </li>
                            <li className="o-layout__tile">
                                <TileNav
                                    to={'/work/yell'}
                                    title="Yell - Maps"
                                    name="yell"
                                />
                            </li>
                            <li className="o-layout__tile">
                                <TileNav
                                    to={'/work/opentoexport'}
                                    title="Open to export"
                                    name="opentoexport"
                                />
                            </li>
                        </ul>
                    </nav>


                </main>

            </section>
        );
    }

}

export default Work;
