import React, { Component } from 'react';
import Helmet from 'react-helmet';

class About extends Component {
    render() {
        const meandbooksImage = require('./meandbooks.png');
        return (
            <section className="o-layout__container">
                <Helmet
                    title="PaulRudge.codes - About"
                />
                <main role="main" className="o-layout__main">

                    <h1>
                        About
                    </h1>

                    <dl className="c-definition-list">
                        <dt className="c-definition-list__term">Name</dt>
                        <dd className="c-definition-list__description">Paul Rudge</dd>

                        <dt className="c-definition-list__term">Age</dt>
                        <dd className="c-definition-list__description">34</dd>

                        <dt className="c-definition-list__term">Current job</dt>
                        <dd className="c-definition-list__description">Senior frontend developer @
                            <a href="https://www.gumtree.com" target="_blank">Gumtree</a>
                        </dd>
                    </dl>

                    <h2>Personal Details</h2>

                    <p>
                        I always strive to be strong in both design and development as it allows
                        me to produce a stronger more integrated end product whether
                        I am working in a team or on my own. I also strive to have as
                        much knowledge about the backend languages and systems I am
                        working with so that I can work more easily alongside backend developers.
                    </p>

                    <p>I am very passionate about developing high quality CSS and love working
                       with methodologies such as BEM, OOCSS and SMACSS. Making sure the sites I
                       generate are as fully accessible as possible has always been important to me
                       and I am always keen to expand my knowledge in areas such as Aria.
                    </p>

                    <p>When I’m not coding or messing around
                        on <a href="http://codepen.io/fakeplasticpaul/" target="_blank">Codepen</a> with
                        my CSS only experiments, you will find me reading science fiction or
                        fantasy novels or watching Anime.
                    </p>

                    <h2>Work Experience</h2>
                    <h3>Gumtree - Senior Frontend Developer</h3>
                    <p>June 2013 - Current Job</p>
                    <p>Whilst working at Gumtree I have taken the lead role in a series of projects
                       to modernize the company's range through the introduction of responsive
                       design across all of there public facing sites as well as a full company
                       rebrand. As part of these projects I introduced new build systems like
                       grunt, new architectural ideas like SMACSS and advocated high levels of
                       code quality, unit test coverage, accessibility and performance. I helped
                       achieve this through the introduction or monitoring tools, workshops and
                       mentoring.
                    </p>

                    <h4>Core skills</h4>
                    <p>HTML5, CSS3, SCSS, SMACSS, JavaScript, Jasmine, React, Freemarker, Thymeleaf,
                       JSP, Grunt, Accessibility, Responsive, Styleguide, Maven, Spring MVC, Scrum,
                       Ab testing, GIT, Jenkins
                    </p>

                    <p>During my time at Gumtree I have been recognised with 4 extra mile
                       awards and a Critical talent award.
                    </p>

                    <p>
                        For more details about my career
                        history <a href="/pdf/CV_Paul_Rudge.pdf">download my cv</a>
                    </p>


                </main>
                <aside className="o-layout__secondary">
                    <div className="u-text--center">
                        <img
                            src={meandbooksImage}
                            role="presentation"
                        />
                    </div>

                </aside>
            </section>
        );
    }

}

export default About;
