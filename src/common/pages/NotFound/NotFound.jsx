import Helmet from 'react-helmet';

const React = require('react');
export default () => <div className="o-layout__container">
    <main role="main" className="o-layout__main">
        <Helmet
            title="PaulRudge.codes - 404"
        />
        <h1>404</h1>
        <p>The page you where looking for can not be found.</p>
    </main>
</div>;

