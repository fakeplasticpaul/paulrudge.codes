import React from 'react';
import { IndexLink, Link } from 'react-router';

const Header = () => (
    <header className="c-header" role="banner">
        <nav role="navigation" aria-label="Main navigation">
            <ul className="c-header__nav">
                <li className="c-header__logo">
                    <IndexLink to={'/'} tabIndex="-1">
                        <svg
                            x="0px"
                            y="0px"
                            width="48px"
                            height="65px"
                            viewBox="0 0 48 65"
                        >
                            <polygon fill="#50514F" points="16,36 16,65 0,65 0,0 48,0 48,36" />
                            <polyline fill="#F25F5C" points="16,36 48,36 41.396,47 30,47" />
                            <rect x="17" y="12" fill="#FFFFFF" width="16" height="11" />
                            <polyline fill="#D64D4D" points="30,47 30,60.299 16,65 16,36" />
                            <polyline fill="#F25F5C" points="17,12 33,12 33,20 27,20 " />
                            <polyline fill="#D64D4D" points="27,20 27,23 17,23 17,12 " />
                        </svg>
                        <span className="u-hide--visually">Paul Rudge codes</span>
                    </IndexLink>
                </li>
                <li className="c-header__tab">
                    <Link
                        className="c-header__link"
                        activeClassName="c-header__link--active"
                        to={'/about'}
                    >
                        About
                    </Link>
                </li>
                <li className="c-header__tab">
                    <Link
                        className="c-header__link"
                        activeClassName="c-header__link--active"
                        to={'/work'}
                    >
                        Work
                    </Link>
                </li>
                <li className="c-header__tab">
                    <Link
                        className="c-header__link"
                        activeClassName="c-header__link--active"
                        to={'/contact'}
                    >
                        Contact
                    </Link>
                </li>
            </ul>
        </nav>
    </header>
);

export default Header;
