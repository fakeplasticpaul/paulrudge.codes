import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const TileNav = ({ title, name, to }) => (
    <Link className={`c-tile-nav c-tile-nav--${name}`} to={to}>
        <h2 className="c-tile-nav__title">{title}</h2>
    </Link>
);

TileNav.propTypes = {
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired
};

export default TileNav;
