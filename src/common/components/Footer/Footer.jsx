import React from 'react';
import { Link } from 'react-router';

const Footer = () => (
    <footer className="c-footer" role="contentinfo">
        <nav role="navigation" aria-label="Footer navigation">
            <ul className="c-footer__nav">
                <li className="c-footer__tab">
                    <Link className="c-footer__link" to={'contact'}>Contact me</Link>
                </li>
                <li className="c-footer__tab">
                    <a className="c-footer__link" href="https://bitbucket.org/fakeplasticpaul/paulrudge.codes" target="_blank">View the code</a>
                </li>
                <li className="c-footer__tab">
                    <Link className="c-footer__link" to={'disclaimer'}>Disclaimer</Link>
                </li>
            </ul>
        </nav>
    </footer>
);

export default Footer;
