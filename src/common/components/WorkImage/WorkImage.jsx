import React, { PropTypes } from 'react';

const WorkImage = ({ image, caption }) => (
    <figure>
        <img src={image} alt={caption} />
        <figcaption>{caption}</figcaption>
    </figure>
);

WorkImage.propTypes = {
    image: PropTypes.string.isRequired,
    caption: PropTypes.string.isRequired
};

export default WorkImage;
