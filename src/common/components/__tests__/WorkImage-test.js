import React from 'react';
import ReactDOM from 'react-dom';
import WorkImage from '../WorkImage/WorkImage';
import { renderIntoDocument } from 'react-addons-test-utils';
import { expect } from 'chai';

describe('WorkImage component', () => {
    const workimageComponent = renderIntoDocument(<div>
        <WorkImage image="test.jpg" caption="test caption" />
    </div>);
    const dom = ReactDOM.findDOMNode(workimageComponent);

    it('should render correctly', () => expect(workimageComponent).to.be.ok);

    it('Make sure the class is set correctly with the name', () => {
        const workimageSrc = dom.getElementsByTagName('img')[0].src;
        expect(workimageSrc).to.have.string('test.jpg');
    });

    it('Make sure the caption is set correctly', () => {
        const workimageCaption = dom.getElementsByTagName('figcaption')[0];
        expect(workimageCaption.innerText).to.have.string('test caption');
    });
});
