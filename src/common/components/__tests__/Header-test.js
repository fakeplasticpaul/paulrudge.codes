import React from 'react';
import ReactDOM from 'react-dom';
import Header from '../Header/Header';
import { renderIntoDocument } from 'react-addons-test-utils';
import { expect } from 'chai';

describe('Header component', () => {
    const headerComponent = renderIntoDocument(<div><Header /></div>);

    const dom = ReactDOM.findDOMNode(headerComponent);

    it('should render correctly', () => expect(headerComponent).to.be.ok);

    it('should render with correct amount of links', () => {
        const headerTabs = dom.getElementsByClassName('c-header__tab');
        expect(headerTabs.length).to.equal(3);
    });

    it('should render the logo', () => {
        const headerLogo = dom.getElementsByClassName('c-header__logo');
        expect(headerLogo.length).to.equal(1);
    });
});
