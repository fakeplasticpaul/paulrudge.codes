import React from 'react';
import ReactDOM from 'react-dom';
import Footer from '../Footer/Footer';
import { renderIntoDocument } from 'react-addons-test-utils';
import { expect } from 'chai';

describe('Footer component', () => {
    const footerComponent = renderIntoDocument(<div><Footer /></div>);

    const dom = ReactDOM.findDOMNode(footerComponent);

    it('should render correctly', () => expect(footerComponent).to.be.ok);

    it('should render with correct amount of links', () => {
        const footerTabs = dom.getElementsByClassName('c-footer__tab');
        expect(footerTabs.length).to.equal(3);
    });
});
