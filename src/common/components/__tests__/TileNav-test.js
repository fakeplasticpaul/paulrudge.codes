import React from 'react';
import ReactDOM from 'react-dom';
import TileNav from '../TileNav/TileNav';
import { renderIntoDocument } from 'react-addons-test-utils';
import { expect } from 'chai';

describe('TileNav component', () => {
    const tilenavComponent = renderIntoDocument(<div>
        <TileNav title="test title" name="testname" to="/about" />
    </div>);
    const dom = ReactDOM.findDOMNode(tilenavComponent);

    it('should render correctly', () => expect(tilenavComponent).to.be.ok);

    it('Make sure the class is set correctly with the name', () => {
        const tileNavLink = dom.getElementsByClassName('c-tile-nav--testname');
        expect(tileNavLink.length).to.equal(1);
    });

    it('Make sure the title is set correctly', () => {
        const tileNavTitle = dom.getElementsByClassName('c-tile-nav__title');
        expect(tileNavTitle[0].innerText).to.equal('test title');
    });
});
