let ReactGA = false;
if (typeof window !== 'undefined') {
    ReactGA = require('react-ga');  // eslint-disable-line global-require
    ReactGA.initialize('UA-79601614-1');
}

export default function promiseMiddleware() {
    return (next) => (action) => {
        if (ReactGA && action.type === '@@router/LOCATION_CHANGE') {
            ReactGA.pageview(action.payload.pathname);
        }

        return next(action);
    };
}
