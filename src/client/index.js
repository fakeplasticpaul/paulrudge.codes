import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import configureStore from '../common/utils/configureStore';
import routes from '../common/routes/routing';
import { syncHistoryWithStore } from 'react-router-redux';
import 'babel-polyfill';

let state = null;
if (window.$REDUX_STATE) {
    state = window.$REDUX_STATE;
}

const store = configureStore(state);

const history = syncHistoryWithStore(browserHistory, store);

render(
    <Provider store={store}>
        <Router history={history} routes={routes} />
    </Provider>,
    document.querySelector('.container')
);

