import express from 'express';
import path from 'path';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { RouterContext, match } from 'react-router';
import routes from '../common/routes/routing';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import promiseMiddleware from '../common/middlewares/AnalyticsMiddleware';
import combinedReducers from '../common/reducers';
import Helmet from 'react-helmet';
import html from '../common/utils/html';

global.DEVELOPMENT = process.env.NODE_ENV !== 'production';

const finalCreateStore = applyMiddleware(promiseMiddleware)(createStore);
const app = express();
const webpack = require('webpack');
let config;
let webpackDevMiddleware;
let webpackHotMiddleware;

if (global.DEVELOPMENT) {
    config = require('../../webpack.config.js');

    app.use('/assets', express.static(path.join(__dirname, '../../client/assets')));
    webpackDevMiddleware = require('webpack-dev-middleware');
    webpackHotMiddleware = require('webpack-hot-middleware');
} else {
    config = require('../../webpack.prod.config.js');

    app.use(express.static(path.join(__dirname, '../../', 'static')));
}

const compiler = webpack(config);

if (global.DEVELOPMENT) {
    app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
    app.use(webpackHotMiddleware(compiler));
}

app.use((req, res) => {
    if (DEVELOPMENT) {
        webpackIsomorphicTools.refresh();
    }

    const store = finalCreateStore(combinedReducers);

    match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
        if (error) {
            return res.status(500).send(error.message);
        }

        if (redirectLocation) {
            return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
        }

        if (renderProps == null) {
            return res.status(404).send('Not found');
        }

        const initView = renderToString((
            <Provider store={store}>
                <RouterContext {...renderProps} />
            </Provider>
        ));

        const head = Helmet.rewind();

        const assets = webpackIsomorphicTools.assets();

        const state = JSON.stringify(store.getState());

        return res.status(200).send(html(initView, state, head, assets));
    });
});

app.get('*', (req, res) => {
    res.status(404).send('Server.js > 404 - Page Not Found');
});

app.use((err, req, res) => {
    console.error('Error on request %s %s', req.method, req.url);
    console.error(err.stack);
    res.status(500).send('Server error');
});

process.on('uncaughtException', evt => {
    console.error('uncaughtException: ', evt);
});

app.listen(process.env.PORT, () => {
    console.log(`Listening on port ${process.env.PORT}`);
});
