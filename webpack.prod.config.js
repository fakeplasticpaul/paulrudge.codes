require('babel-polyfill');

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const WebpackIsomorphicToolsPlugin = require('webpack-isomorphic-tools/plugin');
const webpackIsomorphicToolsPlugin =
    new WebpackIsomorphicToolsPlugin(require('./webpack-isomorphic-tools-configuration'));

module.exports = {

    entry: [
        './src/client/index.js'
    ],

    //
    output: {
        path: path.join(__dirname, 'static/dist'),
        filename: 'bundle-[hash].js',
        publicPath: '/dist/'
    },

    //
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new webpack.optimize.DedupePlugin(),
        new ExtractTextPlugin('styles-[hash].css'),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('production')
            }
        }),
        webpackIsomorphicToolsPlugin
    ],

    resolve: {
        alias: {},
        extensions: ['', '.js', '.jsx']
    },

    module: {
        preLoaders: [
            {
                test: /\.jsx?$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(sass|scss)$/,
                loader: 'stylelint'
            }
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel',
                exclude: /node_modules/,
                include: __dirname,
                query: {
                    presets: ['es2015', 'stage-0', 'react'],
                    plugins: ['transform-decorators-legacy']
                }
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract(
                    'style',
                    'css?importLoaders=2&sourceMap!postcss' +
                    '!sass?sourceMap=true&sourceMapContents=true'
                )
            },

            {
                test: webpackIsomorphicToolsPlugin.regular_expression('images'),
                loader: 'file-loader'
            }
        ]
    },
    postcss: [autoprefixer({ browsers: ['ie > 8,last 2 versions'] })]
};
