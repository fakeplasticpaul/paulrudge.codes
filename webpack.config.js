const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const WebpackIsomorphicToolsPlugin = require('webpack-isomorphic-tools/plugin');
const webpackIsomorphicToolsPlugin =
    new WebpackIsomorphicToolsPlugin(require('./webpack-isomorphic-tools-configuration'))
      .development();

module.exports = {

    entry: [
        'webpack-hot-middleware/client',
        './src/client/index.js'
    ],

    output: {
        path: path.join(__dirname, 'static/dist'),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },

    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        webpackIsomorphicToolsPlugin

    ],

    resolve: {
        alias: {},
        extensions: ['', '.js', '.jsx']
    },

    module: {
        preLoaders: [
            {
                test: /\.jsx?$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(sass|scss)$/,
                loader: 'stylelint'
            }
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel',
                exclude: /node_modules/,
                include: __dirname,
                query: {
                    presets: ['react-hmre', 'es2015', 'stage-0', 'react'],
                    plugins: ['transform-decorators-legacy']
                }
            },
            {
                test: /\.scss$/,
                loader: 'style!css?importLoaders=2&sourceMap!' +
                        'postcss!sass?sourceMap=true&sourceMapContents=true'
            },

            {
                test: webpackIsomorphicToolsPlugin.regular_expression('images'),
                loader: 'url-loader'
            }

        ]
    },
    postcss: [autoprefixer({ browsers: ['ie > 8,last 2 versions'] })]
};
